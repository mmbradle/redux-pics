import compression from 'compression';
var bodyParser = require('body-parser');

var path = require('path');
import express from 'express';
import webpack from 'webpack';
import config from './webpack.config.dev';

var app = express();
var compiler = webpack(config);

app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));


import commentRouter from './server/routes/comment.routes'
import postRouter from './server/routes/post.routes'
import userRouter from './server/routes/user.routes'

app.use(compression());
app.use(bodyParser.json({limit: '20mb'}));
app.use(bodyParser.urlencoded({limit: '20mb', extended: false}));
app.use('/api/comment', commentRouter);
app.use('/api/post', postRouter);
app.use('/api/user', userRouter);

app.get('/health', function(req, res) {
    res.send('hello world');
});


app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname, 'index.html'));
});

app.listen(7770, 'localhost', function (err) {
    if (err) {
        console.log(err);
        return;
    }
    console.log('Listening at http://localhost:7770');
});
