import _ from 'lodash';
import cuid from 'cuid';
import slug from 'limax';
import sanitizeHtml from 'sanitize-html';

import commentsFromData from '../data/comments';

var jsonError = {success: false};
var jsonSuccess = {success: false};
var comments = commentsFromData;


export function getComments(req, res) {
    if (req.query.limit >= 0) {
        res.json(comments.slice(0, req.query.limit));
    } else {
        res.json(comments);
    }
}

export function addComment(req, res) {
    var id = req.params.id;
    var newComment = req.body;
    var postComments = comments[id];
    if (!postComments) {
        postComments = [];
        comments[id] = postComments;
    }
    comments[id].push(newComment);
    res.status(201).json(newComment);
}

export function getComment(req, res) {
    var id = req.params.id;
    var comment = comments[id];
    if (!comment) {
        res.status(404).json(jsonError);
    } else {
        res.json(comment);
    }
}

export function deleteComment(req, res) {
    var id = Number(req.params.id);
    _.remove(comments, {id: id});
    res.json(jsonSuccess);
}
