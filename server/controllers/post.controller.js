import cuid from 'cuid';
import slug from 'limax';
import sanitizeHtml from 'sanitize-html';
import _ from 'lodash';

import postsFromData from '../data/posts';

var posts = _.keyBy(postsFromData, 'code');

var jsonError = {success: false};
var jsonSuccess = {success: true};

export function getPosts(req, res) {
    if (req.query.limit >= 0) {
        res.json(posts.slice(0, req.query.limit));
    } else {
        res.json(posts);
    }
}

export function addPost(req, res) {

}

export function incrementLikes(req, res) {
    var id = req.params.id;
    var post = posts[id];
    post.likes += 1;
    res.json(post);
}

export function getPost(req, res) {
    var id = req.params.id;
    var post = posts[id];
    if (!post) {
        res.status(404).json(jsonError);
    } else {
        res.json(post);
    }
}

export function deletePost(req, res) {

}
