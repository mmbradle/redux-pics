import cuid from 'cuid';
import slug from 'limax';
import sanitizeHtml from 'sanitize-html';
import _ from 'lodash';
import {driver} from '../data/config';

var jsonSuccess = {success: true};

export function getUsers(req, res) {
    var session = driver.session();

    session.run('match (n:Person) return n')
        .then(function (result) {
            var objArr = [];
            result.records.forEach(function (record) {
                // objArr.push(record._fields[0].properties);
                // objArr.push({
                //     name: record.get('n.name'),
                //     email: record.get('n.email')
                // });
                objArr.push(record.get('n').properties);
            });
            res.json(objArr);
            session.close();
        })
        .catch(function (error) {
            console.log(error);
        });
}