import {Router} from 'express';
import * as CommentController from '../controllers/comment.controller';
const router = Router();

// Get all Comments
router.get('/', CommentController.getComments);

// Add a new Comment
router.post('/:id', CommentController.addComment);

// Get one comment by id
router.get('/:id', CommentController.getComment);

// Delete a comment by id
router.delete('/:id', CommentController.deleteComment);

export default router;
