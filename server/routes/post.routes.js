import {Router} from 'express';
import * as PostController from '../controllers/post.controller';
const router = new Router();

// Get all Posts
router.route('/').get(PostController.getPosts);

// Add a new Post
router.route('/').post(PostController.addPost);

// Increment Likes on a Post
router.route('/:id/like').post(PostController.incrementLikes);

// Get one post by id
router.route('/:id').get(PostController.getPost);

// Delete a post by id
router.route('/:id').delete(PostController.deletePost);

export default router;
