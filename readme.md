# Redux Pics

A simple React + Redux implementation.

## Running

First `yarn install` to grab all the necessary dependencies. 

Then run `yarn start` and open <localhost:7770> in your browser.
