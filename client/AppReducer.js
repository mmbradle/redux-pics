import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';

import posts from './reducers/PostReducers';
import comments from './reducers/CommentReducer';

const rootReducer = combineReducers({posts, comments, routing: routerReducer});

export default rootReducer;
