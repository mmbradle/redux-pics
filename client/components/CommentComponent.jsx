import React from 'react';

function CommentComponent(props) {
    function handleRemove (){
        props.removeComment(props.params.postId, props.i);
    }

    return (
        <div className="comment">
            <p>
                <strong>{props.comment.user}</strong>
                {props.comment.text}
                <button className="remove-comment" onClick={handleRemove}>&times;</button>
            </p>
        </div>
    );
}

export default CommentComponent;