import React from 'react';

import CommentComponent from './CommentComponent';

function CommentListComponent(props, context) {
    var api = new React.Component(props, context);

    function handleSubmit(e) {
        e.preventDefault();
        const {postId} = props.params;
        const author = api.refs.author.value;
        const comment = api.refs.comment.value;
        api.props.addComment(postId, author, comment);
        api.refs.commentForm.reset();
    }

    api.render = function () {
        return (
            <div className="comment">
                {api.props.postComments.map((comment, index) => {
                        return <CommentComponent {...api.props} key={index} i={index} comment={comment}/>
                    }
                )}
                <form className="comment-form" ref="commentForm" onSubmit={handleSubmit}>
                    <input type="text" ref="author" placeholder="author"/>
                    <input type="text" ref="comment" placeholder="comment"/>
                    <input type="submit" hidden/>
                </form>
            </div>
        );
    };

    return api;
}

export default CommentListComponent;