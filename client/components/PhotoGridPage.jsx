import React from 'react';

import PhotoComponent from './PhotoComponent';

function PhotoGrid(props, context) {
    var api = new React.Component(props, context);

    api.render = function() {
        var posts = api.props.posts;
        var allComments = api.props.comments;
        var getCommentCount = function(code) {
            var comments = allComments[posts[code].code];
            return comments ? comments.length : 0
        };
        const postKeys = Object.keys(posts);
        return (
            <div className="photo-grid">
                {
                    postKeys.map((code, index) => {
                        return <PhotoComponent {...api.props} key={code} post={posts[code]} commentCount={getCommentCount(code)}/>
                    })
                }
            </div>
        );
    };

    api.componentDidMount = function() {
        api.props.fetchPosts();
        api.props.fetchComments();
    };

    return api;
}

export default PhotoGrid;
