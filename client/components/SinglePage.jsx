import React from 'react';
import {Link} from 'react-router';

import PhotoComponent from './PhotoComponent';
import CommentListComponent from './CommentListComponent';

function Single(props, context) {
    var api = new React.Component(props, context);
    var postId = props.params.postId;

    api.render = function () {
        var posts = api.props.posts;
        var allComments = api.props.comments;
        const post = api.props.posts[postId];
        const postComments = api.props.comments[postId] || [];
        var getCommentCount = function(code) {
            var comments = allComments[posts[code].code];
            return comments ? comments.length : 0
        };
        if (post) {
            return (
                <div className="single-photo">
                    <PhotoComponent {...api.props} key={postId} post={posts[postId]} commentCount={getCommentCount(postId)}/>
                    <CommentListComponent postComments={postComments} {...api.props} />
                </div>
            )
        } else {
            return null;
        }
    };

    api.componentDidMount = function() {
        api.props.fetchPost(postId);
        api.props.fetchComment(postId);
    };

    return api;
}

export default Single;
