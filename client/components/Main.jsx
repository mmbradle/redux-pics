import React from 'react';
import {Link} from 'react-router';

function Main(props, context) {
    var api = new React.Component(props, context);

    api.render = function () {
        return (
            <div>
                <h1>
                    <Link to="/">Reduxstagram</Link>
                </h1>
                {React.cloneElement(api.props.children, api.props)}
            </div>
        );
    };

    return api;
}

export default Main;
