import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import Main from './Main'
import * as postActions from '../actions/PostActions';
import * as commentActions from '../actions/CommentActions';

const allActions = Object.assign({}, postActions, commentActions);

function mapStateToProps(state) {
    return {
        posts: state.posts,
        comments: state.comments
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators(allActions, dispatch);
}

const App = connect(mapStateToProps, mapDispatchToProps)(Main);

export default App;