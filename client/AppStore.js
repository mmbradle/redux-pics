import {createStore, compose, applyMiddleware} from 'redux';
import {syncHistoryWithStore} from 'react-router-redux';
import {browserHistory} from 'react-router';
import thunk from 'redux-thunk';

import rootReducer from './AppReducer';

const defaultState = {
    posts: {},
    comments: {}
};

// Add support for redux-dev-tools
const enhancers = compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
)

// var createStoreWithMiddleware = Redux.applyMiddleware(thunkMiddleware)(Redux.createStore);

const Store = createStore(rootReducer, defaultState, enhancers);

export const history = syncHistoryWithStore(browserHistory, Store);

// Setup hot-reloading of reducers
if (module.hot) {
    module.hot.accept('./AppReducer', () => {
        const nextRootReducer = require('./AppReducer').default;
        Store.replaceReducer(nextRootReducer);
    })
}

export default Store;