import keyMirror from 'keyMirror';

const verb = keyMirror({

    COMMENT_ADD_ONE: null,
    COMMENT_REMOVE_ONE: null,
    COMMENT_REPLACE_ALL: null,

    POST_INCREMENT_LIKES: null,
    POST_REPLACE_ALL: null,

    ZEE_END: null
});

export default verb;