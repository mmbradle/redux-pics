import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import {Router, Route, IndexRoute, browserHistory} from 'react-router'

import css from './styles/style.styl';

import App from './components/App'
import PhotoGrid from './components/PhotoGridPage'
import Single from './components/SinglePage'
import store, {history} from './AppStore';


// Setup Raven (sentry.io) stuff
// import Raven from 'raven-js';
// import {sentry_url}from './data/config';
// Raven.config(sentry_url).install();

const router = (
    <Provider store={store}>
        <Router history={history}>
            <Route path="/" component={App}>
                <IndexRoute component={PhotoGrid}/>
                <Route path="/view/:postId" component={Single}/>
            </Route>
        </Router>
    </Provider>
);

render(
    router,
    document.getElementById('root')
);
