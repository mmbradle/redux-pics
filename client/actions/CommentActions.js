import axios from 'axios';

import verb from '../constants/Verbs';

export function addComment(postId, author, comment) {
    var url = `/api/comment/${postId}`;
    axios.post(url, {user: author, text: comment});
    return {
        type: verb.COMMENT_ADD_ONE,
        postId,
        author,
        comment
    }
}

export function removeComment(postId, i) {
    return {
        type: verb.COMMENT_REMOVE_ONE,
        postId,
        i
    }
}

export function replaceComments(comments) {
    return {
        type: verb.COMMENT_REPLACE_ALL,
        comments
    };
}

export function fetchComments(postId) {
    return (dispatch) => {
        var url = `/api/comment`;
        return axios.get(url).then(res => {
            dispatch(replaceComments(res.data));
        });
    };
}

export function fetchComment(postId) {
    return (dispatch) => {
        var url = `/api/comment/${postId}`;
        return axios.get(url).then(res => {
            var key = postId;
            var value = res.data;
            var newObj = {[key]: value};
            dispatch(replaceComments(newObj));
        });
    };
}

