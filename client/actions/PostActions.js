import axios from 'axios';

import verb from '../constants/Verbs';

export function increment(postId) {
    var url = `/api/post/${postId}/like`;
    axios.post(url);
    return {
        type: verb.POST_INCREMENT_LIKES,
        postId
    }
}

export function replacePosts(posts) {
    return {
        type: verb.POST_REPLACE_ALL,
        posts
    };
}

export function fetchPosts() {
    return (dispatch) => {
        var url = '/api/post';
        return axios.get(url).then(res => {
            dispatch(replacePosts(res.data));
        });
    };
}

export function fetchPost(postId) {
    return (dispatch) => {
        var url = `/api/post/${postId}`;
        return axios.get(url).then(res => {
            var key = postId;
            var value = res.data;
            var newObj = {[key]: value};
            dispatch(replacePosts(newObj));
        });
    };
}
