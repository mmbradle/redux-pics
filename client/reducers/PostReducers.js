import _ from 'lodash';

import verb from '../constants/Verbs';

function postReducers(state = {}, action) {
    switch (action.type) {
        case verb.POST_INCREMENT_LIKES :
            const postId = action.postId;
            var newState = _.cloneDeep(state);
            newState[postId].likes += 1;
            return newState;
        case verb.POST_REPLACE_ALL :
            return action.posts;
        default:
            return state;
    }
}

export default postReducers;