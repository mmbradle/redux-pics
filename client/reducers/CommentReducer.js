import verb from '../constants/Verbs';

function postComments(state = {}, action) {
    switch (action.type) {
        case verb.COMMENT_ADD_ONE:
            return [...state, {
                user: action.author,
                text: action.comment
            }];
        case verb.COMMENT_REPLACE_ALL:
            return [...(action.comment)];
        case verb.COMMENT_REMOVE_ONE:
            return [
                ...state.slice(0, action.i),
                ...state.slice(action.i + 1)
            ];
        default:
            return state;
    }
}

function commentReducer(state = {}, action) {
    if (action.type === verb.COMMENT_REPLACE_ALL) {
        return action.comments;
    } else if (typeof action.postId !== 'undefined') {
        return {
            // take the current state
            ...state,
            // overwrite this post with a new one
            [action.postId]: postComments(state[action.postId], action)
        }
    }
    return state;
}

export default commentReducer;