import Raven from 'raven-js';


const sentry_key = '94251030222c4580877c1fdfcfa0da49';
const sentry_app = '102975';
const sentry_slug = 'sentry.io';

export const sentry_url = `https://${sentry_key}@${sentry_slug}/${sentry_app}`;

export function logException(ex, context) {
    Raven.captureException(ex, {
        extra: context
    });
    /*eslint no-console:0*/
    window && window.console && console.error && console.error(ex);
}
